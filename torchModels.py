import torch

activationDict = torch.nn.ModuleDict([
    ['lrelu', torch.nn.LeakyReLU()],
    ['relu', torch.nn.ReLU()],
    ['softmax',torch.nn.Softmax()],
    ['sigmoid',torch.nn.Sigmoid()],
    ['tanh',torch.nn.Tanh()],
])


def model_multiClass(inputShape, outputDim, outputAct, hiddenSpaceSizes=[50], activations=['relu']):
    """! 
    This function assumes that we will be using a linear multiclass model.
    """
    modules = [torch.nn.Linear(inputShape, hiddenSpaceSizes[0]), activationDict[activations[0]]]
    
    for hiddenSpaceSizeI in range(1, len(hiddenSpaceSizes)-1):
        modules.extend([torch.nn.Linear(hiddenSpaceSizes[hiddenSpaceSizeI], hiddenSpaceSizes[hiddenSpaceSizeI+1]), activationDict[activations[hiddenSpaceSizeI]]])
    modules.extend([torch.nn.Linear(hiddenSpaceSizes[-1], outputDim)])
    modules.extend([activationDict[outputAct]])
    model = torch.nn.Sequential(*modules)
               
    return model

# Try label swapping (real is fake and fake is real)
def model_gan_gen(inputShape, outputDim, outputAct, hiddenSpaceSizes=[50], activations=['relu'], parallel=False):
    """! 
    This function assumes that we will be using a linear multiclass model.
    """
    modules = [torch.nn.Linear(inputShape, hiddenSpaceSizes[0]), activationDict[activations[0]]]
    
    for hiddenSpaceSizeI in range(1, len(hiddenSpaceSizes)-1):
        modules.extend([torch.nn.Linear(hiddenSpaceSizes[hiddenSpaceSizeI], hiddenSpaceSizes[hiddenSpaceSizeI+1]), activationDict[activations[hiddenSpaceSizeI]]])
    
    modules.extend([torch.nn.Linear(hiddenSpaceSizes[-1], outputDim)])
    #modules.extend([activationDict[outputAct]])
    if parallel:
        model = torch.nn.DataParallel(torch.nn.Sequential(*modules)).cuda()
    else:
        model = torch.nn.Sequential(*modules)
               
    return model


def model_gan_discr(inputShape, outputDim, outputAct, hiddenSpaceSizes=[50], activations=['relu'], parallel=False):
    """! 
    This function assumes that we will be using a linear multiclass model.
    """
    modules = [torch.nn.Linear(inputShape, hiddenSpaceSizes[0]), activationDict[activations[0]]]
    for hiddenSpaceSizeI in range(1, len(hiddenSpaceSizes)-1):
        modules.extend([torch.nn.Linear(hiddenSpaceSizes[hiddenSpaceSizeI], hiddenSpaceSizes[hiddenSpaceSizeI+1]), torch.nn.Dropout(0.3), activationDict[activations[hiddenSpaceSizeI]]])

    modules.extend([torch.nn.Linear(hiddenSpaceSizes[-1], outputDim)])
    modules.extend([activationDict[outputAct]])
    if parallel:
        model = torch.nn.DataParallel(torch.nn.Sequential(*modules)).cuda()
    else:
        model = torch.nn.Sequential(*modules)
                  
    return model
