#!/usr/bin/env python
"""@package docstring
This script trains an NN given an h5 file with object kinematics. 
"""

import sys, os, argparse, h5py, matplotlib, math
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from keras.models import model_from_json

# Import all models and turn it into a dict. Not super elegant. All models start with the prefix 'model_'. We grab 
import models
modelDict = {}
for comp in dir(models):
    if 'model_' in comp:
        modelDict[comp] = eval('models.'+comp)

from utils import *

def train(data, modelName, modelFPath, hyperParams, sourceDSName, targetDSName, epochs, batchSize, hyperParamKey, sourceType):
    """! 
    This function trains a model given an h5 file with a training source and target data set. 
    The source is the data set that you want to make look more like the target, e.g. truth vs reco.
    """
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, recoJetsTrain, recoJetsTest, yTrain, yTest, inputScaler, resScalers, unmatchedValue) = data
    jSONSavePath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.json'
    weightsSavePath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.h5'
    model = modelDict[modelName](xTrain.shape[1:], hyperParams['outputDim'], jSONSavePath,
                                 outputAct=hyperParams['outputAct'],
                                 hiddenSpaceSize=hyperParams['hiddenSpaceSize'],
                                 activations=hyperParams['activations']
    )
    history = model.fit(xTrain, yTrain, epochs=epochs, batch_size=batchSize, shuffle=True, validation_data=(xTest, yTest, uniformityWeightsTest), sample_weight=uniformityWeightsTrain)
    model.save_weights(weightsSavePath)

    plt.clf()
    fig, ax = plt.subplots()
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    ax.set_xlabel('epoch')
    ax.set_ylabel('Loss')
    leg = plt.legend(['train', 'test'], loc=0, borderaxespad=0.)
    plt.savefig('loss_'+modelName+'_'+sourceType+'.pdf', bbox_inches='tight')


def test(data, modelName, modelFPath, sourceDSName, targetDSName, hyperParamKey, sourceType, nResCats, printSum=False, nonResIndexStart=4, ratioIndexEnd=2, phiIndex=3):
    """! 
    This function tests a model given an h5 file with a training source and target data set. 
    """
    print("Loading data")
    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, recoJetsTrain, recoJetsTest, yTrain, yTest, inputScaler, resScalers, unmatchedValue) = data
    print("Done loading data")
    binning = [i*0.01 for i in range(101)]
    profileBinning = [i*0.05 for i in range(25)]
    fourVecBinning = [i*0.05 for i in range(21)]

    corrTruthTest = np.copy(xTest)
    corrTruthTrain = np.copy(xTrain)

    # We are assuming that we will only correct the four first elements.
    # There probably is a more elegant way of doing this. 
    corrFactsTest = np.ones((corrTruthTest.shape[0], nonResIndexStart))
    corrFactsTrain = np.ones((corrTruthTrain.shape[0], nonResIndexStart))
    corrFactsTestIndex = np.ones((corrTruthTest.shape[0], nonResIndexStart))
    corrFactsTrainIndex = np.ones((corrTruthTrain.shape[0], nonResIndexStart))

    # We have a model for each resolution we want to model. Loop over resolutions and make predictions that will be
    # store in numpy arrays. 
    modelJSONPath    = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.json'
    modelWeightsPath = modelFPath+sourceDSName+'_'+targetDSName+'_'+modelName+'_'+hyperParamKey+'_'+sourceType+'.h5'
    json_file        = open(modelJSONPath, 'r')
    model_json       = json_file.read()
    json_file.close()
    model = model_from_json(model_json)
    # Load weights into new model
    model.load_weights(modelWeightsPath)
    print("Done loading from", modelJSONPath, modelWeightsPath)
    if printSum:
        model.summary()
    print("Predicting on test and training sample of size", xTest.shape, xTrain.shape, ".")
    yGenTestTotal = model.predict(xTest)
    yGenTrainTotal = model.predict(xTrain)
    print("Done predicting.")

    lastCount = 0
    binsPerFeature = []
    for fourVecI in range(nonResIndexStart):
        fourVecName       = fourVecNames[fourVecI]
        prettyVarLabel    = ratioLabels[fourVecI]
        rmsLabel          = rmsLabels[fourVecI]
        fourVecLabel      = fourVecLabels[fourVecI]
        fourVecLabelTruth = fourVecLabelsTruth[fourVecI]
        
        # The first resolution will also have an inefficiency bin that will tell us whether an objects was reconstructed at all.
        if fourVecI == 0:
            extraBin = 1
        else:
            extraBin = 0
            
        # We now have to split the pdfs into the individual feature pdfs.
        nResBins = nResCats+extraBin
        binsPerFeature.append(nResBins)
        yGenTestTemp = np.copy(yGenTestTotal[:,lastCount:lastCount+nResBins])
        yGenTrainTemp = np.copy(yGenTrainTotal[:,lastCount:lastCount+nResBins])
        
        yTestTemp = np.copy(yTest[:,lastCount:lastCount+nResBins])
        yTrainTemp = np.copy(yTrain[:,lastCount:lastCount+nResBins])
        lastCount += nResBins
        if fourVecI == 0:
            # yGenTestTemp[:,1:] *= (nonResIndexStart)
            # yGenTrainTemp[:,1:] *= (nonResIndexStart)
               
            # yTestTemp[:,1:] *= nonResIndexStart
            # yTrainTemp[:,1:] *= nonResIndexStart
            print nonResIndexStart+1
            print yTestTemp.shape, yGenTestTemp.shape
            print 'bambam', np.sum(yGenTrainTemp[:,1:]), np.sum(yGenTrainTemp[:,0]), np.sum(yGenTrainTemp[:,0])/np.sum(yGenTrainTemp[:,1:])
            print 'bobo', np.sum(yTrainTemp[:,1:]), np.sum(yTrainTemp[:,0]), np.sum(yTrainTemp[:,0])/np.sum(yTrainTemp[:,1:])
            print 'bambam1', np.sum(yGenTestTemp[:,1:]), np.sum(yGenTestTemp[:,0]), np.sum(yGenTestTemp[:,0])/np.sum(yGenTestTemp[:,1:])
            print 'bobo1', np.sum(yTestTemp[:,1:]), np.sum(yTestTemp[:,0]), np.sum(yTestTemp[:,0])/np.sum(yTestTemp[:,1:])
            
            #sys.exit()

        compareDists([np.sum(yTestTemp, axis=0),np.sum(yGenTestTemp, axis=0)], ['Delphes', 'NN'],
                     np.array(range(yGenTestTemp.shape[1]+1)), ['k', 'red'], 'jet',
                     'splitRes_'+fourVecName+'_PDFTest_log', ratioLabels[fourVecI]+' pdf', plotRatio=False, log=True)
        compareDists([np.sum(yTestTemp, axis=0),np.sum(yGenTestTemp, axis=0)], ['Delphes', 'NN'],
                     np.array(range(yGenTestTemp.shape[1]+1)), ['k', 'red'], 'jet',
                     'splitRes_'+fourVecName+'_PDFTest', ratioLabels[fourVecI]+' pdf', plotRatio=False, log=False)
        
        # Now let's get the real and generated reco plots. I'm not sure how
        # to do this in a more numpy way (with indexing instead of looping).
        print("Getting conversion factors")
        corrFactsTestIndex[:,fourVecI] = choice(yGenTestTemp)
        corrFactsTrainIndex[:,fourVecI] = choice(yGenTrainTemp)
        print("Done getting conversion factor.")

    # Get corrected jets.
    # The first index of the first feature always has the inefficiency.
    unmatchedIndicesTest = corrFactsTestIndex[:,0] == 0
    unmatchedIndicesTrain = corrFactsTrainIndex[:,0] == 0

    # We need to calculate the correction factors which should be categoryNumber/nCategories.
    # Special care needs to be taken with the first feature index since it contains the
    # inefficiency bins.
    corrFactsTest = np.zeros((yGenTestTotal.shape[0], nonResIndexStart))
    # The first pdf is the pT with the inefficiency.
    corrFactsTest[:,0] = 1.*(corrFactsTestIndex[:,0]-1)/nResCats;
    corrFactsTest[:,1:] = 1.*corrFactsTestIndex[:,1:]/nResCats
    origScaledCorrFactsTest = resScalers.inverse_transform(corrFactsTest[~unmatchedIndicesTest])
    corrTruthTest[~unmatchedIndicesTest,:ratioIndexEnd] *= origScaledCorrFactsTest[:,:ratioIndexEnd]
    corrTruthTest[~unmatchedIndicesTest,ratioIndexEnd:nonResIndexStart] += origScaledCorrFactsTest[:,ratioIndexEnd:nonResIndexStart]
    corrTruthTest[unmatchedIndicesTest,:nonResIndexStart] = unmatchedValue
    
    corrFactsTrain = np.zeros((yGenTrainTotal.shape[0], nonResIndexStart))
    corrFactsTrain[:,0] = (corrFactsTrainIndex[:,0]-1)/nResCats;
    corrFactsTrain[:,1:] = corrFactsTrainIndex[:,1:]/nResCats
    origScaledCorrFactsTrain = resScalers.inverse_transform(corrFactsTrain[~unmatchedIndicesTrain])
    corrTruthTrain[~unmatchedIndicesTrain,:ratioIndexEnd] *= origScaledCorrFactsTrain[:,:ratioIndexEnd]
    corrTruthTrain[~unmatchedIndicesTrain,ratioIndexEnd:nonResIndexStart] += origScaledCorrFactsTrain[:,ratioIndexEnd:nonResIndexStart]
    corrTruthTrain[unmatchedIndicesTrain,:nonResIndexStart] = unmatchedValue
    
    # # Scale jets back to original scales.
    corrTruthTestOrigScale  = inputScaler.inverse_transform(corrTruthTest)
    corrTruthTrainOrigScale = inputScaler.inverse_transform(corrTruthTrain)
    
    truthTestOrigScale  = inputScaler.inverse_transform(xTest)
    truthTrainOrigScale = inputScaler.inverse_transform(xTrain)

    recoJetsTestOrigScale  = inputScaler.inverse_transform(recoJetsTest)
    recoJetsTrainOrigScale = inputScaler.inverse_transform(recoJetsTrain)
    
    truthJetsTestOrigScale  = inputScaler.inverse_transform(xTest)
    truthJetsTrainOrigScale = inputScaler.inverse_transform(xTrain)

    lowEtaIdx = abs(truthTestOrigScale[:,2]) < 0.4
    highEtaIdx = abs(truthTestOrigScale[:,2]) > 0.8
    
    #realRes = np.argmax(yTest, axis=1)*1.0/yTest.shape[1]
    #genRes  = np.argmax(yGenTestTotal, axis=1)*1.0/yGenTest.shape[1]

    #realResOrigShape = resScalers.inverse_transform(realRes)
    #genResOrigShape  = resScalers.inverse_transform(genRes)

    lowEtaRealResHist = np.sum(yTest[lowEtaIdx,1:nResCats+1], axis=0)
    highEtaRealResHist = np.sum(yTest[highEtaIdx,1:nResCats+1], axis=0)

    lowEtaGenResHist = np.sum(yGenTestTotal[lowEtaIdx,1:nResCats+1], axis=0)
    highEtaGenResHist = np.sum(yGenTestTotal[highEtaIdx,1:nResCats+1], axis=0)

    realResTestHist = np.sum(yTest, axis=0)
    genResTestHist  = np.sum(yGenTestTotal, axis=0)
    
    realResTrainHist = np.sum(yTrain, axis=0)
    genResTrainHist  = np.sum(yGenTrainTotal, axis=0)

    rebinFact = 5
    lowEtaGenResHistRebin = lowEtaGenResHist.reshape(-1,rebinFact).sum(axis=1)
    highEtaGenResHistRebin = highEtaGenResHist.reshape(-1,rebinFact).sum(axis=1)
    lowEtaRealResHistRebin = lowEtaRealResHist.reshape(-1,rebinFact).sum(axis=1)
    highEtaRealResHistRebin = highEtaRealResHist.reshape(-1,rebinFact).sum(axis=1)
    
    # This really ugly.
    rebinning = [i*0.05 for i in range(21)]
    compareDists([lowEtaRealResHistRebin, lowEtaGenResHistRebin, highEtaRealResHistRebin, highEtaGenResHistRebin],
                 ['Delphes, $|\eta|<0.4$', 'NN, $|\eta|<0.4$', 'Delphes, $|\eta|>0.8$', 'NN, $|\eta|>0.8$'],
                 np.array(rebinning), ['k', 'k', 'r', 'r'], 'jet', 'pTResLowEta', ratioLabels[0]+' [scaled]', ratioYLabel="Delphes/NN",
                 markerstyles=['o', '+', 'o', '+'], markerfacecolors=['none', 'none', 'none', 'none'], plotRatio=False, normalize=True, yLogLowRange=0.01)
    compareDists([lowEtaRealResHistRebin, lowEtaGenResHistRebin, highEtaRealResHistRebin, highEtaGenResHistRebin],
                 ['Delphes, $|\eta|<0.4$', 'NN, $|\eta|<0.4$', 'Delphes, $|\eta|>0.8$', 'NN, $|\eta|>0.8$'],
                 np.array(rebinning), ['k', 'k', 'r', 'r'], 'jet', 'pTResLowEta', ratioLabels[0]+' [scaled]', ratioYLabel="Delphes/NN",
                 markerstyles=['o', '+', 'o', '+'], markerfacecolors=['none', 'none', 'none', 'none'], plotRatio=False, log=False,
                 plotHist=[False, True, False, True], plotErr=[True, False, True, False], normalize=True)
   
    pdfBinning = [i*0.01 for i in range(402)]
    compareDists([realResTestHist, genResTestHist], ['Delphes', 'NN'], np.array(pdfBinning), ['k', 'r'], 'jet',
                 'fullResPDFTest', 'pdf', ratioYLabel="Delphes/NN", normalize=False, plotRatio=False)
    compareDists([realResTestHist, genResTestHist], ['Delphes', 'NN'], np.array(pdfBinning), ['k', 'r'], 'jet',
                 'fullResPDFTest', 'pdf', log=False, ratioYLabel="Delphes/NN", normalize=False, plotRatio=False)

    # Let's plot the NN output for some random objects.
    randEvts = np.random.randint(xTest.shape[0], size=3)
    plt.clf()
    fig, ax = plt.subplots()
    for randI in range(len(randEvts)):
        ax.plot([i for i in range(yGenTestTotal.shape[1])], yGenTestTotal[randEvts[randI],:], label='Rand jet '+str(randI), linewidth=5)
    ax.set_xlabel('NN category')
    ax.set_ylabel('Category probability')
    ax.legend(loc=0, borderaxespad=0.)
    plt.savefig('nnOutput.pdf', bbox_inches='tight')
    ax.set_yscale('log')
    yRange = (0.0001, ax.get_ylim()[1])
    ax.set_ylim(yRange)
    plt.savefig('nnOutput_log.pdf', bbox_inches='tight')

    # Now let's make some plots.
    for fourVecI in range(yTrain.shape[1]):
        fourVecName       = fourVecNames[fourVecI]
        prettyVarLabel    = ratioLabels[fourVecI]
        rmsLabel          = rmsLabels[fourVecI]
        fourVecLabel      = fourVecLabels[fourVecI]
        fourVecLabelTruth = fourVecLabelsTruth[fourVecI]
        
        # First in the ML scaling.
        recoJetsTestHist, fourVecBinEdges =  np.histogram(recoJetsTest[:,fourVecI], fourVecBinning)
        print recoJetsTest.shape
        truthJetsTestHist, fourVecBinEdges =  np.histogram(xTest[:,fourVecI], fourVecBinning)
        corrTruthTestHist, fourVecBinEdges =  np.histogram(corrTruthTest[:,fourVecI], fourVecBinning)
        
        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['Delphes', 'NN', 'Generator'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco', fourVecLabel,
                     ratioEndIndex=1, ratioYLabel="Delphes/NN")
        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['Delphes', 'NN', 'Generator'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco', fourVecLabel,
                     log=False, ratioEndIndex=1, ratioYLabel="Delphes/NN")

        # Now in the original.
        recoJetsTestHist, fourVecBinEdges =  np.histogram(recoJetsTestOrigScale[:,fourVecI], origScaleBinning[fourVecI])
        truthJetsTestHist, fourVecBinEdges =  np.histogram(truthJetsTestOrigScale[:,fourVecI], origScaleBinning[fourVecI])
        corrTruthTestHist, fourVecBinEdges =  np.histogram(corrTruthTestOrigScale[:,fourVecI], origScaleBinning[fourVecI])

        recoJetsTrainHist, fourVecBinEdges =  np.histogram(recoJetsTrainOrigScale[:,fourVecI], origScaleBinning[fourVecI])
        truthJetsTrainHist, fourVecBinEdges =  np.histogram(truthJetsTrainOrigScale[:,fourVecI], origScaleBinning[fourVecI])
        corrTruthTrainHist, fourVecBinEdges =  np.histogram(corrTruthTrainOrigScale[:,fourVecI], origScaleBinning[fourVecI])

        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['Delphes', 'NN', 'Generator'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_origScale_test', fourVecLabel, rounding=varRounding[fourVecI], units=units[fourVecI], ratioEndIndex=1, ratioYLabel="Delphes/NN")
        compareDists([recoJetsTestHist, corrTruthTestHist, truthJetsTestHist], ['Delphes', 'NN', 'Generator'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_origScale_test', fourVecLabel, log=False, rounding=varRounding[fourVecI], units=units[fourVecI], ratioEndIndex=1, ratioYLabel="Delphes/NN")
        print "woo orig scale."
        compareDists([recoJetsTrainHist, corrTruthTrainHist, truthJetsTrainHist], ['Delphes', 'NN', 'Generator'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_origScale_train', fourVecLabel, rounding=varRounding[fourVecI], units=units[fourVecI], ratioEndIndex=1, ratioYLabel="Delphes/NN")
        compareDists([recoJetsTrainHist, corrTruthTrainHist, truthJetsTrainHist], ['Delphes', 'NN', 'Generator'],
                     fourVecBinEdges, ['k', 'b', 'r'], 'jet', fourVecName+'_genVsReco_origScale_train', fourVecLabel, log=False, rounding=varRounding[fourVecI], units=units[fourVecI], ratioEndIndex=1, ratioYLabel="Delphes/NN")
        print "Done\n\n"

        # Plot the mean and standard deviation as a function of the variable we are considering.
        # compareMeans(recoJetsTest[:,fourVecI],
        #              [realRes[:,fourVecI], genRes[:,fourVecI]],
        #              ['Real', 'Generator'], np.array(profileBinning), ['k', 'r'], 'jet', fourVecName+'Res',
        #              fourVecLabel.replace('[GeV]', ''),log=False, yLabel=meanLabels[fourVecI])
        # compareRMSs([recoJetsTest[:,fourVecI], corrTruthTest[:,fourVecI]], ['Real', 'Generator'],
        #             np.array(profileBinning), ['k', 'r'], 'jet', fourVecName+'Res', fourVecLabel.replace('[GeV]', ''),log=False,
        #             yLabel=resLabels[fourVecI])

        # Now in the original scaling
        # compareMeans(recoJetsTestOrigScale[:,fourVecI],
        #              [realResOrigShape[:,fourVecI], genResOrigShape[:,fourVecI]],
        #              ['Real', 'Generator'], np.array(origScaleBinning[fourVecI]), ['k', 'r'], 'jet', fourVecName+'ResOrigScale',
        #              fourVecLabel,log=False, yLabel=meanLabels[fourVecI], yRange=(0.5, 1.5))
        # compareRMSs([recoJetsTestOrigScale[:,fourVecI], corrTruthTestOrigScale[:,fourVecI]], ['Real', 'Generator'],
        #             np.array(origScaleBinning[fourVecI]), ['k', 'r'], 'jet', fourVecName+'ResOrigScale', fourVecLabel,log=False,
        #             yLabel=resLabels[fourVecI])

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='Train a NN for regression.')
    parser.add_argument('trainTest', help="Perform training, testing, or both.")
    parser.add_argument('--h5Path', help='Location of training data files.', default='rfast008.h5')
    parser.add_argument('--sourceType', help='What is the source of the data: Delphes or toy detector.', default='Delphes', choices=['Delphes', 'toy', 'other'])
    parser.add_argument('--nFiles', type=int, help='How many files do we want to process', default=None)
    parser.add_argument('--nObjects', type=int, help='Number of objects', default=2000000)
    parser.add_argument('--modelName', help='Name of model.', default='model_multiClass')
    parser.add_argument('--sourceDSName', help='Name of source data set (the data set you want to transform).', default='truthJets')
    parser.add_argument('--targetDSName', help='Name of target data set (the data set you want another dataset to mimic).', default='recoJets')
    parser.add_argument('--modelFPath', default='models/', help='Path where model should be saved.')
    parser.add_argument('--nTrainSamp', type=int, help='Number of objects to use for training', default=200000)
    parser.add_argument('--nTestSamp', type=int, help='Number of objects to use for testing', default=100000)
    parser.add_argument('--epochs', type=int, help='Number of epochs', default=20)
    parser.add_argument('--batchSize', type=int, help='Batch size', default=100)
    parser.add_argument('--nonResIndexStart', type=int, help='What is the index where non resolution variables start', default=4)
    parser.add_argument('--randomSeed', type=int, help='Seed for randomizor', default=18)
    parser.add_argument('--nResCats', type=int, help='Number of resolution categories', default=100)
    parser.add_argument('--uniformityProp', type=int, help='Property which will be flattened during training. For example, we may want a flat pT distribution.', default=None)
    parser.add_argument('--hyperParamFPath', type=str, help='Path to pickle file containing the hyper parameters', default='hyperParamsTest.pkl')
    parser.add_argument('--hyperParamKey', type=str, help='Suffix added to file names. Useful for hyperparameter scan.', default='resMultiCBig')
    parser.add_argument('--updateHyperparams', action='store_true', help='Update the hyperparameter pickle file.')
   
    args = parser.parse_args()

    # Let's load the hyperparameter set we need.
    if args.updateHyperparams:
        print("Producing new hyperparameter file.")
        produceHyperParams(args.hyperParamFPath)
        allHyperParams = pickle.load(open(args.hyperParamFPath))
    else:
        try:
            allHyperParams = pickle.load(open(args.hyperParamFPath))
        except IOError as e:
            print("Couldn't load "+args.hyperParamFPath)
            print("Producing new hyperparameter file.")
            produceHyperParams(args.hyperParamFPath)
            allHyperParams = pickle.load(open(args.hyperParamFPath))
    
    if args.hyperParamKey not in allHyperParams:
        print("Couldn't find hyperparameter set with key"), args.hyperParamKey
        sys.exit()
        
    hyperParams = allHyperParams[args.hyperParamKey]

    np.random.seed(args.randomSeed)
    # Load training and testing data
    data = prepMatchedDataResNew(args.h5Path, args.nTrainSamp, args.nTestSamp, args.randomSeed, sourceType=args.sourceType, nFiles=args.nFiles, nObjects=args.nObjects,uniformityProp=args.uniformityProp, nonResIndexStart=args.nonResIndexStart, nResCats=args.nResCats);
   
    if 'train' in args.trainTest:
        train(data, args.modelName, args.modelFPath, hyperParams, args.sourceDSName, args.targetDSName,
              args.epochs, args.batchSize, args.hyperParamKey, args.sourceType
        );
    if 'test' in args.trainTest:
        test(data, args.modelName, args.modelFPath, args.sourceDSName, args.targetDSName,
             args.hyperParamKey, args.sourceType, args.nResCats
        );
    

