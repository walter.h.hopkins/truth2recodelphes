#!/usr/bin/env python

import sys, os, argparse, h5py, matplotlib, math, torch
import numpy as np
from keras.optimizers import Adam, RMSprop,SGD
from sklearn.externals import joblib 

from copy import deepcopy
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from sklearn.preprocessing import MinMaxScaler, QuantileTransformer, RobustScaler
import scipy.stats


from toyDetector import *

from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import pickle
from glob import glob
from plottingTools import *

varRounding = []

def setTrainability(model, trainable=False):
    model.trainable = trainable
    for layer in model.layers:
        layer.trainable = trainable
    
def mpiToPi(phi):
    while phi >= math.pi: phi-=2*math.pi;
    while phi < -1*math.pi: phi+=2*math.pi;

    return phi
    
def roundSF(number, significance):
    rounding = -int(math.floor(math.log10(abs(number)))) + (significance - 1)
    return round(number, rounding), rounding

def choice(pdfMatrix, axis=1):
    """! 
    This function randomly samples distributions in a matrix of numpy pdfs. 
    Taken from https://stackoverflow.com/questions/47722005/vectorizing-numpy-random-choice-for-given-2d-array-of-probabilities-along-an-a.
    The function first turns the pdfs into cdfs and then randomly samples the cdf with np.random.rand.
    """
    randomCDFProbs = np.expand_dims(np.random.rand(pdfMatrix.shape[1-axis]), axis=axis)
    return (pdfMatrix.cumsum(axis=axis) > randomCDFProbs).argmax(axis=axis)


def produceHyperParams(hyperParamFPath):
    """! 
    This is just a basic example function that shows the format that hyperparameters are in when given to models.
    The hyperparameters are stored in a 2D dictionary. The first dimension is the key for the hyperparameter 
    combination while the second dimension is the hyperparameter name and value.
    
    For GANs both a generator and discriminator set of hyperparameters need to be defined. This is done
    by adding the _gen or _discr suffix to the key.
    """
    hyperParams = {}

    hyperParams['GAN'] = {}
    hyperParams['GAN']['outputAct_gen'] = 'linear'
    hyperParams['GAN']['hiddenSpaceSize_gen'] = [200 for i in range(5)]
    hyperParams['GAN']['activations_gen'] = ['tanh' for i in range(5)]
    hyperParams['GAN']['trainingRatio'] = 1
    hyperParams['GAN']['genLR'] = 0.0001
    hyperParams['GAN']['discrLR'] = 0.0001
    hyperParams['GAN']['genOptimizer'] = Adam
    hyperParams['GAN']['discrOptimizer'] = Adam
    hyperParams['GAN']['softenFactor'] = 1
    hyperParams['GAN']['outputAct_discr'] = 'sigmoid'
    hyperParams['GAN']['hiddenSpaceSize_discr'] = [50 for i in range(5)]
    hyperParams['GAN']['activations_discr'] = ['relu' for i in range(5)]

    hyperParams['WGAN'] = {}
    hyperParams['WGAN']['outputAct_gen'] = 'linear'
    hyperParams['WGAN']['hiddenSpaceSize_gen'] = [50,50,50]
    hyperParams['WGAN']['activations_gen'] = ['tanh', 'tanh', 'tanh']
    hyperParams['WGAN']['trainingRatio'] = 5
    hyperParams['WGAN']['genLR'] = 0.01
    hyperParams['WGAN']['discrLR'] = 0.01
    hyperParams['WGAN']['genOptimizer'] = RMSprop
    hyperParams['WGAN']['discrOptimizer'] = RMSprop
    hyperParams['WGAN']['softenFactor'] = 0.9
    hyperParams['WGAN']['clipValue'] = 5
    hyperParams['WGAN']['outputAct_discr'] = 'sigmoid'
    hyperParams['WGAN']['hiddenSpaceSize_discr'] = [50,50,50]
    hyperParams['WGAN']['activations_discr'] = ['relu', 'relu', 'relu']

    hyperParams['WGAN_GP'] = {}
    hyperParams['WGAN_GP']['outputAct_gen'] = 'linear'
    hyperParams['WGAN_GP']['hiddenSpaceSize_gen'] = [200,200]
    hyperParams['WGAN_GP']['activations_gen'] = ['tanh', 'tanh']
    hyperParams['WGAN_GP']['trainingRatio'] = 5
    hyperParams['WGAN_GP']['genLR'] = 0.0001
    hyperParams['WGAN_GP']['discrLR'] = 0.0001
    hyperParams['WGAN_GP']['genOptimizer'] = Adam
    hyperParams['WGAN_GP']['discrOptimizer'] = Adam
    hyperParams['WGAN_GP']['softenFactor'] = 1
    hyperParams['WGAN_GP']['gradientPenaltyWeight'] = 10
    hyperParams['WGAN_GP']['outputAct_discr'] = 'sigmoid'
    hyperParams['WGAN_GP']['hiddenSpaceSize_discr'] = [200,200]
    hyperParams['WGAN_GP']['activations_discr'] = ['relu', 'relu']

    hyperParams['directRegression'] = {}
    hyperParams['directRegression']['outputDim'] = 4
    hyperParams['directRegression']['outputAct'] = 'linear'
    hyperParams['directRegression']['hiddenSpaceSize'] = [200,200,200]
    hyperParams['directRegression']['activations'] = ['relu' for i in  hyperParams['directRegression']['hiddenSpaceSize']]

    hyperParams['resTest'] = {}
    hyperParams['resTest']['outputDim'] = 100
    hyperParams['resTest']['outputAct'] = 'sigmoid'
    hyperParams['resTest']['hiddenSpaceSize'] = [50,50]
    hyperParams['resTest']['activations'] = ['relu', 'relu']
    
    hyperParams['resMultiC'] = {}
    hyperParams['resMultiC']['outputDim'] = 500
    hyperParams['resMultiC']['outputAct'] = 'softmax'
    hyperParams['resMultiC']['hiddenSpaceSize'] = [100,100,100,100,100]
    hyperParams['resMultiC']['activations'] = ['relu', 'relu', 'relu','relu','relu']
    
    hyperParams['resMultiC4Layers'] = {}
    hyperParams['resMultiC4Layers']['outputDim'] = 100
    hyperParams['resMultiC4Layers']['outputAct'] = 'softmax'
    hyperParams['resMultiC4Layers']['hiddenSpaceSize'] = [100,100,100,100]
    hyperParams['resMultiC4Layers']['activations'] = ['relu', 'relu', 'relu','relu']

    hyperParams['resMultiCBig'] = {}
    hyperParams['resMultiCBig']['outputDim'] = 401
    hyperParams['resMultiCBig']['outputAct'] = 'softmax'
    hyperParams['resMultiCBig']['hiddenSpaceSize'] = [100,100,100,100,100,100,100]
    hyperParams['resMultiCBig']['activations'] = ['relu' for i in  hyperParams['resMultiCBig']['hiddenSpaceSize']]

    pickle.dump(hyperParams, open(hyperParamFPath, 'wb'))


def prepMatchedData(h5Path, nTrainSamp, nTestSamp, randomSeed, resBins=[i*0.01 for i in range(100)], nFiles=None, sourceType='Delphes', nObjects=2000000, percentiles=[1,99], uniformityProp=0, uniformityBins=[i*20 for i in range(21)]+[500, 600, 700, 800, 1000, 2000], phiIndex=2, nonOutputVarIndexStart=4):
    """! 
    Retrieve matched jets.
    """
    if sourceType == 'Delphes':
        truthObjects, recoObjects = getObjectsFromH5DelphesNew(h5Path, nFiles)
    elif sourceType == 'toy':
        #truthObjects, recoObjects = produceObjects(nObjects=nObjects)
        truthObjects, recoObjects = getObjectsFromText(h5Path)
    else:
        truthObjects, recoObjects = getObjectsFromH5(h5Path, nFiles)

    # Number of histogram bins
    nSteps = 20
    
    # for featI in range(truthObjects.shape[1]):
    #     minVal = min([np.min(recoObjects[:,featI]), np.min(truthObjects[:,featI])])
    #     maxVal = max([np.max(recoObjects[:,featI]), np.max(truthObjects[:,featI])])
    #     stepSize = (maxVal-minVal)*1.0/nSteps;
    #     roundedMax, rounding = roundSF(maxVal, 1)
    #     roundedMin, rounding = roundSF(minVal, 1)
    #     if stepSize > 0:
    #         roundedStepSize, rounding = roundSF(stepSize,1)
    #     else:
    #         roundeStepSize = minVal
            
    #     roundedNSteps = int((roundedMax-roundedMin)/roundedStepSize)
        
    #     tempBinning = [roundedMin+(i-1)*roundedStepSize for i in range(roundedNSteps+5)]
    #     # Phi is special since it goes from -pi to pi. This is bad: usage of magic numbers. Will need to fix later.
    #     if featI == 2:
    #         stepSize = math.pi*2/nSteps
    #         tempBinning = [-math.pi+(i-1)*stepSize for i in range(nSteps+5)]
    #         rounding = 2

    #     varRounding.append(rounding)
    #     origScaleBinning.append(tempBinning)
    
    # Get index of object that haven't been reconstructed.
    # This is indicated by negative mass.
    lostObjIndex = recoObjects[:,0]<0;

    # Scale reco and truth objects to have ranges of 0.1 to 1.1.
    # This allows the resolution (reco/truth) to be defined everywhere.
    minScaledVal = 0
    maxScaledVal = 1
    # Step size for scaled parameters. 
    stepSize = (maxScaledVal-minScaledVal)/nSteps

    # First remove quantities that we don't want to predict in the reco objects.
    recoObjects = recoObjects[:,:nonOutputVarIndexStart]
    inputScaler = MinMaxScaler((minScaledVal, maxScaledVal));
    outputScaler = MinMaxScaler((minScaledVal, maxScaledVal));
    # Don't use unmatched reco objects for scaling.
    matchedRecoObjects = recoObjects[~lostObjIndex];
    matchedRecoObjects[:,phiIndex] = np.sin(matchedRecoObjects[:,phiIndex])
    truthObjects[:,phiIndex] = np.sin(truthObjects[:,phiIndex])
    matchedTruthObjects = truthObjects[~lostObjIndex];
    truthObjectsScaled = inputScaler.fit_transform(truthObjects)
    matchedTruthObjectsScaled = truthObjectsScaled[~lostObjIndex]
    unmatchedTruthObjectsScaled = truthObjectsScaled[lostObjIndex]
    matchedRecoObjectsScaled = outputScaler.fit_transform(matchedRecoObjects)

    # Unmatched objects get put in an underflow-like bin.
    unmatchedValue = minScaledVal-(maxScaledVal-minScaledVal)/nSteps
    unmatchedRecoObjectsScaled = -1.*unmatchedTruthObjectsScaled[:,:nonOutputVarIndexStart]#np.ones(recoObjects[lostObjIndex].shape)*unmatchedValue
    
    recoObjectsScaled = np.concatenate((matchedRecoObjectsScaled, unmatchedRecoObjectsScaled))[:,0:1]
    truthObjectsScaled = np.concatenate((matchedTruthObjectsScaled, unmatchedTruthObjectsScaled))[:,0:1]

    # recoObjectsScaled = matchedRecoObjectsScaled[:,0:1]
    # truthObjectsScaled = matchedTruthObjectsScaled[:,0:1]
    for featI in range(recoObjectsScaled.shape[1]):
        recoObjectValsScaled, tempBinEdgesScaled = np.histogram(recoObjectsScaled[:,featI], [stepSize*(i-3)+minScaledVal for i in range(nSteps+5)])
        truthObjectValsScaled, tempBinEdgesScaled = np.histogram(truthObjectsScaled[:,featI], [stepSize*(i-3)+minScaledVal for i in range(nSteps+5)])
        compareDists([recoObjectValsScaled, truthObjectValsScaled], ['Delphes', 'Gen'], tempBinEdgesScaled,
                     ['k', 'r'], 'jet', featNames[featI]+'_scaled', featLabels[featI].replace('[GeV]', 'scaled'), ratioYLabel="Delphes/Gen", plotRatio=False)
        compareDists([recoObjectValsScaled, truthObjectValsScaled], ['Delphes', 'Gen'], tempBinEdgesScaled,
                     ['k', 'r'], 'jet', featNames[featI]+'_scaled', featLabels[featI].replace('[GeV]', 'scaled'), log=False, ratioYLabel="Delphes/Gen", plotRatio=False)

    uniformityWeights = np.ones((truthObjectsScaled.shape[0]))
    if uniformityProp != None:
        uniformityWeights = flattenDist(truthObjectsScaled[:,uniformityProp], uniformityBins)

    # Add randomness column to mimic stochastic process
    #truthObjectsScaled = np.column_stack((truthObjectsScaled, np.random.rand(truthObjectsScaled.shape[0],1)))

    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest) = train_test_split(truthObjectsScaled, uniformityWeights, recoObjectsScaled, test_size=nTestSamp, train_size=nTrainSamp, random_state=randomSeed)
    
    return (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, yTrain, yTest, inputScaler, outputScaler)
    
def processDelphesObject(objects):
    """!
    Process objects from files converted from Delphes outputs. 
    Currently only handling matched jets."
    """
    recoJetsPt   = np.ndarray.flatten(objects['AntiKt4MatchedJetPt'])
    truthJetsPt  = np.ndarray.flatten(objects['AntiKt4MatchedTruthJetPt'])
    recoJetsEta  = np.ndarray.flatten(objects['AntiKt4MatchedJetEta'])
    truthJetsEta = np.ndarray.flatten(objects['AntiKt4MatchedTruthJetEta'])
    recoJetsPhi  = np.ndarray.flatten(objects['AntiKt4MatchedJetPhi'])
    truthJetsPhi = np.ndarray.flatten(objects['AntiKt4MatchedTruthJetPhi'])
    recoJetsM    = np.ndarray.flatten(objects['AntiKt4MatchedJetM'])
    truthJetsM   = np.ndarray.flatten(objects['AntiKt4MatchedTruthJetM'])

    # Remove NaNs
    recoJetsPt = recoJetsPt[~np.isnan(recoJetsPt)]
    recoJetsEta = recoJetsEta[~np.isnan(recoJetsEta)]
    recoJetsPhi = recoJetsPhi[~np.isnan(recoJetsPhi)]
    recoJetsM = recoJetsM[~np.isnan(recoJetsM)]
    
    truthJetsPt = truthJetsPt[~np.isnan(truthJetsPt)]
    truthJetsEta = truthJetsEta[~np.isnan(truthJetsEta)]
    truthJetsPhi = truthJetsPhi[~np.isnan(truthJetsPhi)]
    truthJetsM = truthJetsM[~np.isnan(truthJetsM)]

    # Create NJets by 4 numpy arrays for reco and truth jets. 
    recoJets = np.zeros((recoJetsPt.shape[0], 4))
    recoJets[:,0] = recoJetsPt
    recoJets[:,1] = recoJetsEta
    recoJets[:,2] = recoJetsPhi
    recoJets[:,3] = recoJetsM
    
    truthJets = np.zeros((truthJetsPt.shape[0], 4))
    truthJets[:,0] = truthJetsPt
    truthJets[:,1] = truthJetsEta
    truthJets[:,2] = truthJetsPhi
    truthJets[:,3] = truthJetsM
    
    return truthJets, recoJets


def getObjectsFromH5Delphes(h5Path, nFiles=None):
    """!
    Get objects from H5 path assuming the name of the data sets have a naming convention particular 
    to our Delphes files.
    """
    inFs = [h5Path]
    if os.path.isdir(h5Path):
        inFs = sorted(glob(h5Path+"*.h5"))

    print("Loading files")
    h5f     = h5py.File(inFs[0], 'r')
    objects = h5f.get('Ntuple')['2d']

    truthJets, recoJets = processDelphesObject(objects)
    for h5fName in inFs[1:nFiles]:
        h5f       = h5py.File(h5fName, 'r')
        objects = h5f.get('Ntuple')['2d']
        print("Grabbing data for", h5fName)
        tempTruthJets, tempRecoJets = processDelphesObject(objects);
        recoJets = np.concatenate([recoJets, tempRecoJets])
        truthJets = np.concatenate([truthJets, tempTruthJets])
    
    return truthJets, recoJets


def getObjectsFromH5DelphesNew(h5Path, nFiles=None, objType=0):
    """!
    Get objects from H5 path assuming the name of the data sets have a naming convention particular 
    to our Delphes files.
    """
    inFs = [h5Path]
    if os.path.isdir(h5Path):
        inFs = sorted(glob(h5Path+"*.h5"))

    print("Loading files")
    h5f     = h5py.File(inFs[0], 'r')
    recoObj = h5f.get('recoObjects')
    truthObj = h5f.get('truthObjects')
    
    recoJets = recoObj[recoObj['type']==objType]
    truthJets = truthObj[truthObj['type']==objType]

    tempRecoJets = recoJets.view(np.float32).reshape(recoJets.shape + (-1,))
    tempTruthJets = truthJets.view(np.float32).reshape(truthJets.shape + (-1,))
    recoJets = tempRecoJets[:,1:]
    truthJets = tempTruthJets[:,1:]
    
    return truthJets, recoJets
    

def getObjectsFromH5(h5Path, nFiles=None):
    """!
    Get objects from H5 path.
    """
    inFs = [h5Path]
    if os.path.isdir(h5Path):
        inFs = sorted(glob(h5Path+"*.h5"))

    print("Loading files")
    h5f       = h5py.File(inFs[0], 'r')
    recoJets  = h5f.get('matchedRecoJets').value
    truthJets = h5f.get('matchedTruthJets').value

    # We want a list of objects, rather than something with shape evI, objI, featI
    recoJets = recoJets.flatten()
    truthJets = truthJets.flatten()
    newShape = (recoJets.shape[0], len(recoJets[0]))
    recoJets = recoJets.view(np.float32).reshape(newShape)
    truthJets = truthJets.view(np.float32).reshape(newShape)

    for h5fName in inFs[1:nFiles]:
        h5f       = h5py.File(h5fName, 'r')
        print("Grabbing data for", h5fName)
        tempRecoJets  = h5f.get('matchedRecoJets').value
        tempTruthJets = h5f.get('matchedTruthJets').value
        
        # We want a list of objects, rather than something with shape evI, objI, featI
        tempRecoJets = tempRecoJets.flatten()
        tempTruthJets = tempTruthJets.flatten()
        newShape = (tempRecoJets.shape[0], len(tempRecoJets[0]))
        recoJets = np.concatenate([recoJets, tempRecoJets.view(np.float32).reshape(newShape)])
        truthJets = np.concatenate([truthJets, tempTruthJets.view(np.float32).reshape(newShape)])
    
    recoJets = recoJets[~np.isnan(recoJets).any(axis=1)]
    truthJets = truthJets[~np.isnan(truthJets).any(axis=1)]

    return truthJets, recoJets


def getObjectsFromText(inFPath, nFiles=None):
    """!
    Get objects from text file where the first line is a header and the following lines
    are alternatingly inputs and outputs.
    """
    #inLines = open(inFPath, 'r').readlines()[1:]
    with open(inFPath, 'r', errors='replace') as f:
        inLines = f.readlines()[1:]
    nInputFeatures = len(inLines[0].split())
    nOutputFeatures = len(inLines[1].split())
    nObjects = len(inLines)/2+1
    inputObjects = np.zeros((nObjects, nInputFeatures))
    outputObjects = np.zeros((nObjects, nOutputFeatures))
    
    for lineI in range(len(inLines)):
        inLine = inLines[lineI]
        features = np.array([float(feature) for feature in inLine.split()])
        if lineI % 2 == 0:
            inputObjects[lineI] = features
        else:
            outputObjects[lineI] = features
            
    return intputObjects, outputObjects
    

    
def prepMatchedDataRes(h5Path, nTrainSamp, nTestSamp, randomSeed, resBins=[i*0.01 for i in range(100)], nFiles=None, sourceType='Delphes', nObjects=2000000, percentiles=[1,99], uniformityProp=0, uniformityBins=[i*.01 for i in range(101)], phiIndex=2, trainingFeatures=[0,1], scalerPath='scaler.pkl', resScalerPath='resScaler.pkl', updateScalers=False, saveTrainingSet=False, diffIndices=[1,2,3]):
    """! 
    Retrieve matched jets.
    """
    if sourceType == 'Delphes':
        truthJets, recoJets = getObjectsFromH5DelphesNew(h5Path, nFiles)
    elif sourceType == 'toy':
        #truthJets, recoJets = produceObjects(nObjects=nObjects)
        truthJets, recoJets = getObjectsFromText(h5Path)
    else:
        truthJets, recoJets = getObjectsFromH5(h5Path, nFiles)
   
    truthJets = truthJets[:,trainingFeatures]
    recoJets = recoJets[:,trainingFeatures]

    # Number of histogram bins
    nSteps = 50

    # Get index of object that haven't been reconstructed.
    # This is indicated by negative mass.
    matchedObjIndex = recoJets[:,0]>0;
    matchedRecoJets = recoJets[matchedObjIndex];
    matchedTruthJets = truthJets[matchedObjIndex];
    
    # The resolution is the difference between reco and truth objects that have been matched.
    # Note that the difference is not between the rescaled reco and truth objects.
    #resolutions = (matchedRecoJets[:,trainingFeatures]-matchedTruthJets[:,trainingFeatures])/matchedTruthJets[:,trainingFeatures]
    resolutions = (matchedRecoJets[:,trainingFeatures]-matchedTruthJets[:,trainingFeatures])/matchedTruthJets[:,trainingFeatures]
    # Finally, we need deal with phi wrapping.

    for diffIndex in diffIndices:
        resolutions[:,diffIndex] = matchedRecoJets[:,diffIndex]-matchedTruthJets[:,diffIndex]

    if phiIndex != None:
        resolutions[:,phiIndex] = np.vectorize(mpiToPi)((matchedRecoJets[:,phiIndex]-matchedTruthJets[:,phiIndex]))
    
    # Make sure that the resolutions are bounded to a reasonable range.
    ranges = np.percentile(resolutions, percentiles, axis=0)
    goodIndices = np.all((resolutions > ranges[0]) & (resolutions < ranges[1]), axis=1)
    
    resolutions = resolutions[goodIndices]
    matchedRecoJets = matchedRecoJets[goodIndices]
    matchedTruthJets = matchedTruthJets[goodIndices]
    (trainIndices, testIndices) = train_test_split(range(matchedTruthJets.shape[0]), test_size=nTestSamp, train_size=nTrainSamp, random_state=randomSeed)

    # Scale reco and truth jets to have ranges of 0 to 1.
    minScaledVal = 0.
    maxScaledVal = 1.
    # Don't fit the scaler if this was already done for a training sample
    if updateScalers:
        scaler = MinMaxScaler((minScaledVal, maxScaledVal));
        scaler.fit(np.concatenate((matchedTruthJets[trainIndices], matchedRecoJets[trainIndices])))
        joblib.dump(scaler, scalerPath) 
    else:
        print('Loading input scaler from '+resScalerPath)
        scaler = joblib.load(scalerPath) 

    matchedTruthJetsScaled = scaler.transform(matchedTruthJets)
    matchedRecoJetsScaled = scaler.transform(matchedRecoJets)

    resScaledBinned = []
    resBinning = []
    
    for resI in range(resolutions.shape[1]):
        plt.clf()
        fig, ax = plt.subplots()
        stepSize = (ranges[1,resI]-ranges[0,resI])/nSteps
        resBinning.append([ranges[0,resI]+i*stepSize for i in range(nSteps)])
        ax.hist(resolutions[:,resI], histtype='step', bins=resBinning[resI])
        ax.set_xlabel(ratioLabels[resI])
        ax.set_ylabel('Jets')
        plt.savefig(resNames[resI]+'_nobounds_prescaling.pdf', bbox_inches='tight')
        plt.savefig(resNames[resI]+'_nobounds_prescaling.svg', bbox_inches='tight')
        plt.savefig(resNames[resI]+'_nobounds_prescaling.eps', bbox_inches='tight')
    plt.close('all')

    # Scale output, i.e. the resolutions. Also put in unmatched objects at lower end of scale.
    resMinScaledVal = 0
    resMaxScaledVal = 1
    
    if updateScalers:
        resScaler = MinMaxScaler((resMinScaledVal, resMaxScaledVal))
        resScaler.fit(resolutions[trainIndices])
        joblib.dump(resScaler, resScalerPath) 
    else:
        print('Loading resolution scaler from '+resScalerPath)
        resScaler = joblib.load(resScalerPath) 

    resScaled = resScaler.transform(resolutions)
    resScaledFilledBins = np.digitize(resScaled, bins=resBins[:-1])
    resScaledBinned = np.zeros((list(resScaled.shape)+[len(resBins)]))
    jetI, elI = np.ogrid[:resScaled.shape[0],:resScaled.shape[1]]
    resScaledBinned[jetI, elI, resScaledFilledBins] = 1

    for resI in range(resolutions.shape[1]):
        plt.clf()
        fig, ax = plt.subplots()
        ax.hist(resScaled[:,resI], histtype='step', bins=resScaledBinning[resI])
        xLabel = ratioLabels[resI]+' [scaled]'
        if '[' in ratioLabels[resI]:
            xLabel = ratioLabels[resI].replace('[GeV]', ' [scaled]')
        ax.set_xlabel(xLabel)
            
        ax.set_ylabel('Jets')
        plt.savefig(resNames[resI]+'_bounds_scaled.pdf', bbox_inches='tight')
        plt.savefig(resNames[resI]+'_bounds_scaled.svg', bbox_inches='tight')
        plt.savefig(resNames[resI]+'_bounds_scaled.eps', bbox_inches='tight')
        
        # Four vectors pre scaling.
        minVal = min([np.min(matchedRecoJets[:,resI]), np.min(matchedTruthJets[:,resI])])
        maxVal = max([np.max(matchedRecoJets[:,resI]), np.max(matchedTruthJets[:,resI])])
        stepSize = (maxVal-minVal)*1.0/nSteps;
        roundedMax, rounding = roundSF(maxVal, 1)
        roundedMin, rounding = roundSF(minVal, 1)
        roundedStepSize, rounding = roundSF(stepSize,1)

        roundedNSteps = int((roundedMax-roundedMin)/roundedStepSize)
        
        tempBinning = [roundedMin+(i-1)*roundedStepSize for i in range(roundedNSteps+5)]
        # Phi is special since it goes from -pi to pi
        if resI == phiIndex:
            stepSize = math.pi*2/nSteps
            tempBinning = [-math.pi+(i-1)*stepSize for i in range(nSteps)]
            rounding = 2

        recoJetVals, tempBinEdges = np.histogram(matchedRecoJets[:,resI], origScaleBinning[resI])
        truthJetVals, tempBinEdges = np.histogram(matchedTruthJets[:,resI], origScaleBinning[resI])
        compareDists([recoJetVals, truthJetVals], ['Delphes', 'Generator'], tempBinEdges, ['k', 'r'], 'jet', featNames[resI]+'_prescaling',
                     featLabels[resI], rounding=rounding, units=units[resI], plotRatio=False, yLabel="Jets")
        compareDists([recoJetVals, truthJetVals], ['Delphes', 'Generator'], tempBinEdges, ['k', 'r'], 'jet', featNames[resI]+'_prescaling',
                     featLabels[resI], log=False, rounding=rounding, units=units[resI], plotRatio=False, yLabel="Jets")

        stepSize = 1.0/nSteps
        recoJetValsScaled, tempBinEdgesScaled = np.histogram(matchedRecoJetsScaled[:,resI], [stepSize*(i-1)+minScaledVal for i in range(nSteps+3)])
        truthJetValsScaled, tempBinEdgesScaled = np.histogram(matchedTruthJetsScaled[:,resI], [stepSize*(i-1)+minScaledVal for i in range(nSteps+3)])
        xLabel = featLabels[resI]+' [scaled]'
        if '[' in featLabels[resI]:
            xLabel = featLabels[resI].replace('[GeV]', '[scaled]')
        compareDists([recoJetValsScaled, truthJetValsScaled], ['Delphes', 'Generator'], tempBinEdgesScaled,
                     ['k', 'r'], 'jet', featNames[resI]+'_scaled', xLabel, plotRatio=False, yLabel="Jets")
        compareDists([recoJetValsScaled, truthJetValsScaled], ['Delphes', 'Generator'], tempBinEdgesScaled,
                     ['k', 'r'], 'jet', featNames[resI]+'_scaled', xLabel, log=False, plotRatio=False, yLabel="Jets")

    plt.close('all')

    uniformityWeights = np.ones((matchedTruthJetsScaled.shape[0]))
    uniformityHist = np.array([])
    if uniformityProp != None:     
        #uniformityWeights = flattenDist(matchedTruthJetsScaled[:,uniformityProp], uniformityBins)
        uniformityWeights, uniformityHist = flattenDist(resScaled[:,uniformityProp], bins=resBins)

    (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, matchedRecoJetsTrain, matchedRecoJetsTest, yTrain, yTest) = matchedTruthJetsScaled[trainIndices], matchedTruthJetsScaled[testIndices], uniformityWeights[trainIndices], uniformityWeights[testIndices],  matchedRecoJetsScaled[trainIndices], matchedRecoJetsScaled[testIndices], resScaledBinned[trainIndices], resScaledBinned[testIndices]

    if saveTrainingSet:
        h5f = h5py.File('trainingSet_'+sourceType+'.h5', 'w')
        h5f.create_dataset('xTrain', data    = xTrain)
        h5f.create_dataset('xTest', data     = xTest)
        for trainFeatI in range(len(trainingFeatures)):
            h5f.create_dataset('y'+featNames[trainingFeatures[trainFeatI]]+'Train', data  = yTrain[:,trainFeatI,:])
            h5f.create_dataset('y'+featNames[trainingFeatures[trainFeatI]]+'Test', data   = yTest[:,trainFeatI,:])
        h5f.close();
    return (xTrain, xTest, uniformityWeightsTrain, uniformityWeightsTest, matchedRecoJetsTrain, matchedRecoJetsTest, yTrain, yTest, scaler, resScaler, minScaledVal, maxScaledVal, resMinScaledVal, resMaxScaledVal, uniformityHist)

def flattenDist(data, bins):
    """! 
    This function gets weights to flatten a distribution. This is mainly meant to avoid overtraining on
    a region that has many more events (such as low pT jets).
    """
    binVals, binEdges = np.histogram(data, bins)
    binVals = np.append(binVals, [data[data>bins[-1]].shape[0]])
    digitized = np.digitize(data, bins[:-1])
    normed = np.ones(binVals.shape, dtype=np.float64)/binVals
    
    # There really should be a more vectorize numpy way to do this. 
    weights = np.array([normed[digitized[i]-1] for i in range(digitized.shape[0])])
    return weights, normed


def transformObjects(inputObjects, model, outputObjects, scaler, resScaler, featureIndex, nResCats, minResVal, maxResVal, modelType='keras', uniformityHist=np.array([])):
    """!
    Transform objects by getting the NN produced PDF and sampling this PDF.
    This function assumes that the inputObjects were scaled by a scaler (also a parameter to the function). 
    It also assumes that the PDF is binned in categories, thus the NN is a multi-categorizing NN.
    """
    
    print("Predicting pdf for object", inputObjects.shape, ".")
    if modelType=='keras':
        pdf = model.predict(inputObjects)
        if uniformityHist.size:
            pdf = pdf/uniformityHist
    else:
        inputObjects = torch.autograd.Variable(torch.from_numpy(inputObjects),requires_grad=False)
        pdf = model(inputObjects)
        pdf = pdf.detach().numpy()
    print("Done predicting pdf.")
    print("Getting conversion factors")
    # Let the user choose what bins to start sampling.
    # This is because this bin only has a meaning in the first feature: counting the inefficiency.
    corrFactsIndex = choice(pdf)
    stepSize = 1.*(maxResVal-minResVal)/nResCats
    corrFacts = (corrFactsIndex+.5)*stepSize
    # To rescale the correction factors they have to be a matrix.
    corrFactsMatrix = np.zeros(outputObjects.shape)
    corrFactsMatrix[:,featureIndex] = corrFacts
    origScaledCorrFacts = resScaler.inverse_transform(corrFactsMatrix)
 
    if modelType=='keras':
        outputObjects[:,featureIndex] = scaler.inverse_transform(inputObjects)[:,featureIndex]*(1+origScaledCorrFacts[:,featureIndex])
    else:
        outputObjects[:,featureIndex] = scaler.inverse_transform(inputObjects.numpy())[:,featureIndex]*(1+origScaledCorrFacts[:,featureIndex])
    return pdf

def calculateMET(fourVecs):
    """! 
    Calculate the vectorial sum of some objects.
    The input is assumed to be series of four vectors that 
    have pT, eta, phi, m as indices.
    """
    
